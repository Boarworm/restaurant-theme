module.exports = {
    mode: 'jit',
    content: [
        "./pages/**/*.{htm,js}",
        "./partials/**/*.{htm,js}",
    ],
    theme: {
        fontFamily: {
            'secondary': ['Acme', 'cursive'],
        },

        container: {
            center: true,
            screens: {
                sm: "100%",
                md: "100%",
                lg: "1400px",
            },
            padding: {
                DEFAULT: '1rem',
                md: '2rem',
                lg: '3rem',
            }
        },
        extend: {},
    },
    plugins: [
        require('@tailwindcss/typography'),
    ],
}
