// Fancybox
const fancybox = require("@fancyapps/fancybox");
$('[data-fancybox="gallery"]').fancybox({
    thumbs: {
        autoStart: true
    }
});

// Components
import '../../../partials/burger/burger'
import '../../../partials/mobile-menu/mobile-menu'
import '../../../partials/google-map/google-map'
import '../../../partials/to-top-button/to-top-button'

// Helpers
import './helpers'
