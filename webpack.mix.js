const mix = require('laravel-mix');

const jsFileList = [
    'assets/src/js/app',
];

let postCssPlugins = [
    require('tailwindcss'),
];

mix.setPublicPath('assets');

mix.setResourceRoot('/themes/restaurant/assets');

mix.options({
        terser: {
            extractComments: false,
        }
    }
);

mix.webpackConfig(webpack => ({
    plugins: [
        new webpack.ProvidePlugin({
            $: require.resolve('jquery'),
            jQuery: require.resolve('jquery'),
            'window.jQuery': require.resolve('jquery'),
            'window.$': require.resolve('jquery'),
        })
    ]
}))

jsFileList.forEach(fileName => mix.js(`./${fileName}.js`, 'assets/dist/js'));

mix.postCss('assets/src/css/app.css', 'assets/dist/css', postCssPlugins);

mix.browserSync({
    proxy: 'localhost:8000',
    open: false,
    reloadDelay: 500,
    files: [
        './content/**/*.htm',
        './layouts/*.htm',
        './pages/**/*.htm',
        './partials/**/*.htm'
    ],
});

mix.sourceMaps(true, 'source-map');
mix.extract(['jquery', 'fancybox']);
mix.version();
